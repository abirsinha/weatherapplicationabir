Technology stack of Front end:
1. Bootstrap v4
2. jQuery along with jQuery validator and jQuery datatable
3. Javascript
4. css

Technology stack of Backend:
1. Java 8
2. Spring Mvc
3. Spring Web-Flow(Registration page)
4. Spring Security(Login page)
5. ORM framework--> Hibernate v5 with Spring ORM
6. Database--> H2
7. Logging framework--> slf4j
8. Validator--> Spring Validator and Hibernate Validator

Test:
1.Junit v4.12
2.Hamcrest v1.3

Application can be accessed at: http://localhost:8080/weatherApplication/login