<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>

<div class="container">

	<div class="row">		

		<div class="col-md-offset-2 col-md-8">
			<div class="panel panel-primary">

				<div class="panel-heading">
					<h4>Weather Information to Update</h4>
				</div>

				<div class="panel-body">
					<!-- Form Elements -->

					<sf:form class="form-horizontal" modelAttribute="weatherHistory"
						action="${contextRoot}/update/product" method="POST">
						<div class="form-group">
							<label class="control-label col-md-4" for="city">City</label>
							<div class="col-md-8">
								<sf:input type="text" path="city" id="city"
									placeholder="City Name" class="form-control" />
									<sf:errors path="city" cssClass="help-block" element="em"/>
							</div>
						</div>



						<div class="form-group">
							<label class="control-label col-md-4" for="currentTemp">Current
								Temperature</label>
							<div class="col-md-8">
								<sf:input type="text" path="currentTemp" id="currentTemp"
									placeholder="Current Temperature" class="form-control" />
									<sf:errors path="currentTemp" cssClass="help-block" element="em"/>
							</div>
						</div>



						<div class="form-group">
							<label class="control-label col-md-4" for="description">Description</label>
							<div class="col-md-8">
								<sf:input type="text" path="description" id="description"
									placeholder="Description" class="form-control" />
									<sf:errors path="description" cssClass="help-block" element="em"/>
							</div>
						</div>


						<div class="form-group">
							<label class="control-label col-md-4" for="minTemp">Minimum
								Temperature</label>
							<div class="col-md-8">
								<sf:input type="text" path="minTemp" id="minTemp"
									placeholder="Minimum Temperature" class="form-control" />
									<sf:errors path="minTemp" cssClass="help-block" element="em"/>
							</div>
						</div>



						<div class="form-group">
							<label class="control-label col-md-4" for="maxTemp">Maximum
								Temperature</label>
							<div class="col-md-8">
								<sf:input type="text" path="maxTemp" id="maxTemp"
									placeholder="Maximum Temperature" class="form-control" />
									<sf:errors path="maxTemp" cssClass="help-block" element="em"/>
							</div>
						</div>



						<div class="form-group">
							<label class="control-label col-md-4" for="sunrise">Sunrise
								Time</label>
							<div class="col-md-8">
								<sf:input type="text" path="sunrise" id="sunrise"
									placeholder="Sunrise" class="form-control" />
									<sf:errors path="sunrise" cssClass="help-block" element="em"/>
							</div>
						</div>



						<div class="form-group">
							<label class="control-label col-md-4" for="sunset">Sunset
								Time</label>
							<div class="col-md-8">
								<sf:input type="text" path="sunset" id="sunset"
									placeholder="Sunset" class="form-control" />
									<sf:errors path="sunset" cssClass="help-block" element="em"/>
							</div>
						</div>


						<div class="form-group">
							<div class="col-md-offset-4 col-md-8">
								<input type="submit" name="submit" id="submit" value="Update"
									class="btn btn-primary" />
								<!-- Hidden Fields -->
								<sf:hidden path="weatherId" />
								<sf:hidden path="userId" />

							</div>
						</div>

					</sf:form>

				</div>

			</div>

		</div>


	</div>



</div>