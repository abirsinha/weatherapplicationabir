<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<spring:url var="css" value="/resources/css" />

<c:set var="contextRoot" value="${pageContext.request.contextPath}" />

<!DOCTYPE html>
<html lang="en">

<head>

<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description" content="Abir's Weather Application">
<meta name="author" content="Abir Sinha">

<!-- Bootstrap Core CSS -->
<link href="${css}/bootstrap.min.css" rel="stylesheet">

<!-- Loading main css file -->
<link rel="stylesheet" href="${css}/style.css">

<!-- Bootstrap Readable Theme -->
<link href="${css}/bootstrap-readable-theme.css" rel="stylesheet">


<title>Abir's Weather Application-${title}</title>

</head>

<body>

	<div class="wrapper">

		<div class="site-header">
			<div class="container">
				<a href="" class="branding"> <img src="${images}/logo.png"
					alt="" class="logo">
					<div class="logo-type">
						<h1 class="site-title" href="${contextRoot}/login">Abir's Weather Report</h1>
						<small class="site-description">Weather Application</small>
					</div>
				</a>
			</div>
		</div>


		<div class="content">

			<div class="container">

				<div class="row">

					<div class="col-xs-12">


						<div class="jumbotron">

							<h1>${errorTitle}</h1>
							<hr />

							<blockquote style="word-wrap: break-word">

								${errorDescription}</blockquote>

						</div>


					</div>

				</div>

			</div>

		</div>


		<%@include file="./shared/footer.jsp"%>

	</div>


</body>


</html>