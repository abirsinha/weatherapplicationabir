<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<spring:url var="css" value="/resources/css" />
<spring:url var="js" value="/resources/js" />
<spring:url var="images" value="/resources/images" />


<c:set var="contextRoot" value="${pageContext.request.contextPath}" />

<html lang="en">
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport"
	content="width=device-width, initial-scale=1.0,maximum-scale=1">
<meta name="description" content="Abir's Weather Application">
<meta name="author" content="Abir Sinha">

<title>Abir's Weather Application-${title}</title>

<script>
	window.menu = '${title}';
	window.contextRoot='${contextRoot}';
</script>


<!-- Loading third party fonts -->
<link href="http://fonts.googleapis.com/css?family=Roboto:300,400,700|"
	rel="stylesheet" type="text/css">
<!-- <link href="fonts/font-awesome.min.css" rel="stylesheet" type="text/css"> -->


<!-- Bootstrap Core CSS -->
<link href="${css}/bootstrap.min.css" rel="stylesheet">



<!-- Loading main css file -->
<link rel="stylesheet" href="${css}/style.css">

<!-- Bootstrap Readable Theme -->
<link href="${css}/bootstrap-readable-theme.css" rel="stylesheet">

</head>


<body>

	<div class="site-header">
		<div class="container">
			<a href="" class="branding"> <img src="${images}/logo.png" alt=""
				class="logo">
				<div class="logo-type">
					<h1 class="site-title">Abir's Weather Report</h1>
					<small class="site-description">Weather Application</small>
				</div>
			</a>
		</div>
	</div>

	<div class="site-content">

		<div class="container">

			<!-- This will be displayed only if user enters invalid credentials -->
			<c:if test="${not empty message}">
				<div class="row">
					<div class="col-md-offset-3 col-md-6">
						<div class="alert alert-danger">${message}</div>
					</div>
				</div>
			</c:if>


			<!-- This will be displayed once user has succesfully logged out -->
			<c:if test="${not empty logout}">
				<div class="row">
					<div class="col-md-offset-3 col-md-6">
						<div class="alert alert-success">${logout}</div>
					</div>
				</div>
			</c:if>

			<div class="row">

				<div class="col-md-offset-3 col-md-6">

					<div class="panel panel-primary">

						<div class="panel-heading">
							<h4>Login</h4>
						</div>

						<div class="panel-body">
							<form action="${contextRoot}/login" method="POST"
								class="form-horizontal" id="loginForm">
								<div class="form-group">
									<label for="username" class="col-md-4 control-label">Email:
									</label>
									<div class="col-md-8">
										<input type="text" name="username" id="username"
											class="form-control" />
									</div>
								</div>
								<div class="form-group">
									<label for="password" class="col-md-4 control-label">Password:
									</label>
									<div class="col-md-8">
										<input type="password" name="password" id="password"
											class="form-control" />
									</div>
								</div>
								<div class="form-group">
									<div class="col-md-offset-4 col-md-8">
										<input type="hidden" name="${_csrf.parameterName}"
											value="${_csrf.token}" /> <input type="submit" value="Login"
											class="btn btn-primary" />
									</div>
								</div>
							</form>

						</div>
						<div class="panel-footer">
							<div class="text-right">
								New User - <a href="${contextRoot}/register">Register Here</a>
							</div>
						</div>


					</div>

				</div>

			</div>

		</div>


		<!-- .site-footer -->
		<%@include file="./shared/footer.jsp"%>

	</div>

	<script src="${js}/jquery-1.11.1.min.js"></script>
	<script src="${js}/jquery.validate.js"></script>
	<script src="${js}/bootstrap.min.js"></script>


	<script src="${js}/app.js"></script>

</body>

</html>