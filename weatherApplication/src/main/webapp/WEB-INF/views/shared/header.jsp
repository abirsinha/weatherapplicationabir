<div class="site-header">
	<div class="container">
		<a href="" class="branding"> <img src="${images}/logo.png" alt=""
			class="logo">
			<div class="logo-type">
				<h1 class="site-title">Abir's Weather Report</h1>
				<small class="site-description">Weather Application</small>
			</div>
		</a>

		<!-- navigation -->
		<%@include file="navbar.jsp"%>

	</div>
</div>