<%@taglib prefix="security"
	uri="http://www.springframework.org/security/tags"%>

<div class="main-navigation">
	<button type="button" class="menu-toggle">
		<i class="fa fa-bars"></i>
	</button>
	<ul class="menu">		

		<security:authorize access="isAnonymous()">
			<li class="menu-item" id="register"><a
				href="${contextRoot}/register">Sign Up</a></li>
			<li class="menu-item" id="login"><a href="${contextRoot}/login">Login</a></li>
		</security:authorize>

		<security:authorize access="isAuthenticated()">
			<li class="menu-item" id="searchCity"><a
				href="${contextRoot}/searchCity">Weather Report</a></li>
			<li class="menu-item" id="logout"><a
				href="${contextRoot}/perform-logout">Logout</a></li>
		</security:authorize>

	</ul>

</div>
