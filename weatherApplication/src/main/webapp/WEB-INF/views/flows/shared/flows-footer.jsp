<!-- .site-footer -->
<%@include file="../../shared/footer.jsp"%>

<script src="${js}/jquery-1.11.1.min.js"></script>
<script src="${js}/jquery.validate.js"></script>
<script src="${js}/bootstrap.min.js"></script>
<script src="${js}/app.js"></script>

</body>

</html>