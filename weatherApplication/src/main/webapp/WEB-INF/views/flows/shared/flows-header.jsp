<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>

<spring:url var="css" value="/resources/css" />
<spring:url var="js" value="/resources/js" />
<spring:url var="images" value="/resources/images" />


<c:set var="contextRoot" value="${pageContext.request.contextPath}" />

<html lang="en">
<head>
<meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport"
	content="width=device-width, initial-scale=1.0,maximum-scale=1">
<meta name="description" content="Abir's Weather Application">
<meta name="author" content="Abir Sinha">

<title>Abir's Weather Application-${title}</title>

<script>
	window.menu = '${title}';
	window.contextRoot='${contextRoot}';
</script>


<!-- Loading third party fonts -->
<link href="http://fonts.googleapis.com/css?family=Roboto:300,400,700|"
	rel="stylesheet" type="text/css">


<!-- Bootstrap Core CSS -->
<link href="${css}/bootstrap.min.css" rel="stylesheet">


<!-- Loading main css file -->
<link rel="stylesheet" href="${css}/style.css">

<!-- Bootstrap Readable Theme -->
<link href="${css}/bootstrap-readable-theme.css" rel="stylesheet">

</head>


<body>

	<div class="site-header">
		<div class="container">
			<a href="" class="branding"> <img src="${images}/logo.png" alt=""
				class="logo">
				<div class="logo-type">
					<h1 class="site-title">Abir's Weather Report</h1>
					<small class="site-description">Weather Application</small>
				</div>
			</a>
		</div>
	</div>