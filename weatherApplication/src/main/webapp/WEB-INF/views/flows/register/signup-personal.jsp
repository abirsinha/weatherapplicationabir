<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>

<%@include file="../shared/flows-header.jsp"%>

<div class="site-content">

	<div class="container">
		<div class="row">
		

		<div class="col-md-offset-2 col-md-8">
			<div class="panel panel-primary">

				<div class="panel-heading">
					<h4>SignUp- New User Registration</h4>
				</div>

				<div class="panel-body">
					<!-- Form Elements -->

					<sf:form class="form-horizontal" modelAttribute="user"
						id="registerForm" method="POST">
						
						<div class="form-group">
							<label class="control-label col-md-4">Email</label>
							<div class="col-md-8">
								<sf:input type="text" path="email" 
									placeholder="Email" class="form-control" />
									<sf:errors path="email" cssClass="help-block" element="em"/>
							</div>
						</div>

						<div class="form-group">
							<label class="control-label col-md-4">Date of Birth</label>
							<div class="col-md-8">
								<sf:input type="text" path="dob" 
									placeholder="Date of Birth" class="form-control" />
									<sf:errors path="dob" cssClass="help-block" element="em"/>
							</div>
						</div>
						
						<div class="form-group">
								<label class="control-label col-md-4">Password</label>
								<div class="col-md-8">
									<sf:input type="password" path="password" class="form-control"
										placeholder="Password" />
									<sf:errors path="password" cssClass="help-block" element="em"/> 
								</div>
							</div>
														
							<div class="form-group">
								<div class="col-md-offset-4 col-md-8">
									<button type="submit" name="_eventId_confirm" class="btn btn-primary">
										Next - Confirm <span class="glyphicon glyphicon-chevron-right"></span>
									</button>																	 
								</div>
							</div>

					</sf:form>

				</div>

			</div>

		</div>


	</div>
	
	</div>

</div>

<%@include file="../shared/flows-footer.jsp"%>