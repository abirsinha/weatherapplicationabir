<%@include file="../shared/flows-header.jsp"%>

<div class="site-content">

	<div class="container">
	<div class="row">
	
		<div class="col-sm-12">
	
			<div class="panel panel-primary">
				
				<div class="panel-heading">
					<h4>Personal Details</h4>
				</div>
			
				<div class="panel-body">
					<div class="text-center">						
						<h4>Email : <strong>${registerModel.user.email}</strong></h4>
						<h4>Date Of Birth : <strong>${registerModel.user.dob}</strong></h4>						
						<p>
							<a href="${flowExecutionUrl}&_eventId_personal" class="btn btn-primary">Edit</a>
						</p>
					</div>
				</div>
			
			</div>
					
		
		</div>
		
		
	
	</div>
	
	<div class="row">
		
		<div class="col-sm-4 col-sm-offset-4">
			
			<div class="text-center">
				
				<a href="${flowExecutionUrl}&_eventId_submit" class="btn btn-lg btn-primary">Confirm</a>
				
			</div>
			
		</div>
		
	</div>
	</div>

</div>

<%@include file="../shared/flows-footer.jsp"%>