
<%@ taglib prefix="sf" uri="http://www.springframework.org/tags/form"%>

<div class="hero" data-bg-image="images/banner.png">
	<div class="container">

		<c:if test="${not empty message}">

			<div class="col-xs-12">
				<div class="alert alert-sucess alert-dismissible">
					<button type="button" class="close" data-dismiss="alert">&times;</button>
					${message}

				</div>
			</div>
		</c:if>


		<sf:form action="${contextRoot}/getCityTemperature"
			class="find-location" modelAttribute="weatherHistory" method="POST" id="searchCityField">
			<sf:input type="text" path="city" placeholder="Find your location..." />
			<input type="submit" value="Find">
			<sf:errors path="city" cssClass="help-block" element="em" />
		</sf:form>

		
			<sf:form action="${contextRoot}/bulkDelete/weatherInfo" 
				modelAttribute="weatherHistory" method="POST">
				<input id="bulkDelete" type="submit" value="Bulk Delete">
			</sf:form>
		

		<div class="row">		

			<table id="userWeatherDetails"
				class="table table-striped table-borderd">

				<thead>
					<tr>				
						<th>City</th>
						<th>Current Temperature</th>
						<th>Description</th>
						<th>Minimum Temperature</th>
						<th>Maximum Temperature</th>
						<th>Sunrise Time</th>
						<th>Sunset Time</th>
						<th>Username</th>
						<th></th>
					</tr>
				</thead>


				<tfoot>
					<tr>		
						<th>City</th>
						<th>Current Temperature</th>
						<th>Description</th>
						<th>Minimum Temperature</th>
						<th>Maximum Temperature</th>
						<th>Sunrise Time</th>
						<th>Sunset Time</th>
						<th>Username</th>
						<th></th>
					</tr>
				</tfoot>


			</table>
		</div>
	</div>

</div>