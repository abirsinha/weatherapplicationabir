$(function() {

	// for handling CSRF token
	var token = $('meta[name="_csrf"]').attr('content');
	var header = $('meta[name="_csrf_header"]').attr('content');

	if ((token != undefined && header != undefined)
			&& (token.length > 0 && header.length > 0)) {
		// set the token header for the ajax request
		$(document).ajaxSend(function(e, xhr, options) {
			xhr.setRequestHeader(header, token);
		});
	}

	switch (menu) {

	case 'home':
		$('#home').addClass('menu-item current-menu-item');
		break;

	case 'Search City':
		$('#searchCity').addClass('menu-item current-menu-item');
		break;

	default:
		$('#a_' + menu).addClass('menu-item current-menu-item');
		break;
	}

	/*The below populates the datatable*/

	var $table = $('#userWeatherDetails');

	if ($table.length > 0) {

		var jsonUri = '';
		jsonUri = window.contextRoot + '/json/data/all/Weather/Report';

		$table
				.DataTable({
					lengthMenu : [ [ 3, 5, 10, -1 ],
							[ '3 Records', '5 Records', '10 Records', 'ALL' ] ],
					pageLength : 3,
					ajax : {
						url : jsonUri,
						dataSrc : ''
					},
					columns : [

							{
								data : 'city'
							},
							{
								data : 'currentTemp',
								mRender : function(data, type, row) {
									return data + '&#8451;'
								}
							},
							{
								data : 'description'
							},
							{
								data : 'minTemp',
								mRender : function(data, type, row) {
									return data + '&#8451;'
								}
							},
							{
								data : 'maxTemp',
								mRender : function(data, type, row) {
									return data + '&#8451;'
								}
							},
							{
								data : 'sunrise'
							},
							{
								data : 'sunset'
							},
							{
								data : 'userId'
							},

							{
								data : 'weatherId',
								bSortable : false,
								mRender : function(data, type, row) {
									var str = '';
									str += '<a href="'
											+ window.contextRoot
											+ '/show/'
											+ data
											+ '/weatherInfo"><button type="button" class="btn btn-info">Update</button></a>'
									str += '<a href="'
											+ window.contextRoot
											+ '/delete/'
											+ data
											+ '/weatherInfo"><button type="button" class="btn btn-danger">Delete</button></a>'
									return str;
								}
							}

					]

				});
	}

	//dismissing the alert after 7 seconds
	var $alert = $('.alert');

	if ($alert.length) {
		setTimeout(function() {
			$alert.fadeOut('slow');
		}, 7000);
	}

	/*Validates the Login form*/
	$loginForm = $('#loginForm');

	if ($loginForm.length) {

		$loginForm.validate({
			rules : {
				username : {
					required : true,
					email : true

				},
				password : {
					required : true
				}
			},
			messages : {
				username : {
					required : 'Please enter your email!',
					email : 'Please enter a valid email address!'
				},
				password : {
					required : 'Please enter your password!'
				}
			},
			errorElement : "em",
			errorPlacement : function(error, element) {
				// Add the 'help-block' class to the error element
				error.addClass("help-block");

				// add the error label after the input element
				error.insertAfter(element);
			}
		}

		);

	}
	
	/*Validates the search City text box*/
	$searchCityField = $('#searchCityField');

	jQuery.validator.addMethod("lettersonly", function(value, element) {
		return this.optional(element) || /^[a-zA-Z]+$/i.test(value);
	}, "Please enter only Alphabets!!");

	if ($searchCityField.length) {

		$searchCityField.validate({
			rules : {
				city : {
					required : true,
					lettersonly : true
				}
			},
			messages : {
				city : {
					required : 'Please enter city name you want to search!'
				}
			},
			errorElement : "em",
			errorPlacement : function(error, element) {
				// Add the 'help-block' class to the error element
				error.addClass("help-block");

				// add the error label after the input element
				error.insertAfter(element);
			}
		}

		);

	}

});

/*Bulk Delete functionality selecting rows to passing Id to be deleted */
$(document).ready(function() {
	var table = $('#userWeatherDetails').DataTable();

	$('#userWeatherDetails tbody').on('click', 'tr', function() {
		$(this).toggleClass('selected');
		var pos = table.row(this).index();
		var row = table.row(pos).data();
		console.log(row);

	});
	
	$('#bulkDelete').click(function() {
		var dataArr = [];
		var rows = $('tr.selected');
		var rowData = table.rows(rows).data();
		$.each($(rowData), function(key, value) {
			dataArr.push(value["weatherId"]); 
		});
		console.log(dataArr);
		
		$.ajax({
			type : "POST",
			url : window.contextRoot + "/bulkDelete/weatherInfo",
			data : {
				myArray : dataArr			
			}

		});
	});

});