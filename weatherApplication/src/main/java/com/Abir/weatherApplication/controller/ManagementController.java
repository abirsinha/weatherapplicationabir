package com.Abir.weatherApplication.controller;

import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.Abir.weatherApplication.validator.WeatherValidator;
import com.Abir.weatherApplicationBackend.dao.WeatherDAO;
import com.Abir.weatherApplicationBackend.dto.WeatherHistory;
import com.Abir.weatherApplicationBackend.model.UserModel;
import com.Abir.weatherApplicationBackend.service.WeatherInformationService;

/**
 * The ManagementController class handles all the functionalities a user
 * performs on its logged in screen like searching City Weather details ,
 * updating , deleting or bulk deleting its information.
 *
 * @author Abir Sinha
 * @version 1.0
 * @since 2019-04-14
 */
@Controller
public class ManagementController {

	private static final Logger logger = LoggerFactory.getLogger(ManagementController.class);

	@Autowired
	private WeatherInformationService weatherInformationService;
	@Autowired
	private WeatherDAO weatherDAO;
	@Autowired
	private HttpSession session;

	/**
	 * This method maps the searchCity page and displays the already searched
	 * datatable information if exists.
	 * 
	 * @param operation
	 *            This is the parameter to capture any message encounterd by
	 *            user action in the page, reported in request param.
	 * @return ModelAndView This returns ModelAndView object with the page name
	 *         and appropriate message.
	 */
	@RequestMapping(value = { "/", "/searchCity" }, method = RequestMethod.GET)
	public ModelAndView submit(@RequestParam(name = "operation", required = false) String operation) {
		logger.info("Inside ManagementController--> submit");
		ModelAndView mv = new ModelAndView("page");
		mv.addObject("title", "Search City");
		mv.addObject("userClicksSearchCity", true);
		WeatherHistory weatherHistory = new WeatherHistory();
		mv.addObject("weatherHistory", weatherHistory);
		if (operation != null) {
			if (operation.equals("successUpdate")) {
				mv.addObject("message", "Weather Details updated sucessfully");
			} else if (operation.equals("successDelete")) {
				mv.addObject("message", "Weather Details deleted sucessfully");
			} else if (operation.equals("cityNotFound")) {
				mv.addObject("message", "Error while fetching Weather Details or provided City name is incorrect!!");
			} else if (operation.equals("cityAlreadyExists")) {
				mv.addObject("message",
						"The city is already present in below table.Please refer below or try searching some other city");
			}
		}
		return mv;
	}

	/**
	 * This method maps the getCityTemperature page and accepts input from user
	 * for the city to be searched.If the city has already been searched by user
	 * it displays the message accordingly or else searches the same and
	 * displays to the user
	 * 
	 * @param weatherHistory
	 *            This is a model attribute parameter.
	 * @return String This returns a redirect String value object with the page
	 *         names after operation.
	 * 
	 */
	@RequestMapping(value = "/getCityTemperature", method = RequestMethod.POST)
	public String getCityTemperature(@ModelAttribute("weatherHistory") WeatherHistory weatherHistory) {
		logger.info("Inside ManagementController--> getCityTemperature");
		System.out.println(weatherHistory.getCity());
		UserModel userModel = (UserModel) session.getAttribute("userModel");
		System.out.println(userModel.getEmail());

		// Convert input city String to first letter uppercase and rest followed
		// by lowercase to maintain data consistency in database
		String city = weatherHistory.getCity().substring(0, 1).toUpperCase()
				+ weatherHistory.getCity().substring(1).toLowerCase();

		// Check if the entered city already been searched by user once
		Integer cityCount = weatherDAO.checkCityExistence(city, userModel.getEmail());
		if (cityCount > 0) {
			return "redirect:/searchCity?operation=cityAlreadyExists";
		}

		// Get searched city weather information
		Integer responseCode = weatherInformationService.getWeatherInformation(city, userModel);
		if (responseCode != 200) {
			return "redirect:/searchCity?operation=cityNotFound";
		}

		return "redirect:/searchCity";
	}

	/**
	 * This method shows the weather information for the selected row from the
	 * datatable.
	 * 
	 * @param PathVariable
	 *            id The id is extracted from the path variable.
	 * @return ModelAndView This returns a modelAndView object with fetched
	 *         details
	 * 
	 */
	@RequestMapping(value = "/show/{id}/weatherInfo", method = RequestMethod.GET)
	public ModelAndView showProduct(@PathVariable int id) {
		logger.info("Inside ManagementController--> showProduct");
		ModelAndView mv = new ModelAndView("page");
		mv.addObject("title", "Update Button");
		mv.addObject("userClicksUpdateButton", true);
		WeatherHistory weatherHistory = weatherDAO.getWeatherInformationById(id);
		mv.addObject("weatherHistory", weatherHistory);
		return mv;
	}

	/**
	 * This method is called during update operation, when user wants to update
	 * any data that has been fetched.
	 * 
	 * @param weatherHistory
	 *            This is a first parameter which is a model attribute
	 *            parameter.
	 * @param BindingResult
	 *            This is the second parameter is for holding bindingResult.
	 * @param Model
	 *            This is the third parameter is for Model object.
	 * @return String This returns a redirect String value object with the page
	 *         names after operation.
	 * 
	 */
	@RequestMapping(value = "/update/product", method = RequestMethod.POST)
	public String updateProduct(@Valid @ModelAttribute("weatherHistory") WeatherHistory weatherHistory,
			BindingResult results, Model model) {
		logger.info("Inside ManagementController--> updateProduct");
		new WeatherValidator().validate(weatherHistory, results);

		// check in case of any error
		if (results.hasErrors()) {
			model.addAttribute("userClicksUpdateButton", true);
			model.addAttribute("title", "Update Button");
			return "page";
		}
		weatherHistory.setCreatedOn(new Date());
		weatherDAO.update(weatherHistory);
		return "redirect:/searchCity?operation=successUpdate";
	}

	/**
	 * This method is called during delete operation, when user wants to delete
	 * any data that has been fetched.
	 * 
	 * @param pathVariable
	 *            This is the parameter which holds pathVariable value.
	 * @return String This returns a redirect String value object with the page
	 *         names after operation.
	 * 
	 */
	@RequestMapping(value = "/delete/{id}/weatherInfo", method = RequestMethod.GET)
	public String deleteProduct(@PathVariable int id) {
		logger.info("Inside ManagementController--> deleteProduct");
		weatherDAO.delete(id);
		return "redirect:/searchCity?operation=successDelete";
	}

	/**
	 * This method is called during bulk delete operation, when user wants to
	 * bulk delete multiple data at once.
	 * 
	 * @param requestParam
	 *            This is the parameter which holds the selected Ids to be
	 *            deleted at once, whcih has been selected by user from front
	 *            end.
	 * @return String This returns a redirect String value object with the page
	 *         names after operation.
	 * 
	 */
	@RequestMapping(value = "/bulkDelete/weatherInfo", method = RequestMethod.POST)
	public String bulkDeleteProduct(@RequestParam(name = "myArray[]", required = false) List<Integer> myArray) {
		logger.info("Inside ManagementController--> bulkDeleteProduct");
		System.out.println(myArray);
		if (myArray != null && !myArray.isEmpty()) {
			weatherDAO.deleteBulkData(myArray);
			return "redirect:/searchCity?operation=successDelete";
		}
		return "redirect:/searchCity";
	}
}
