package com.Abir.weatherApplication.controller;

import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ModelAttribute;

import com.Abir.weatherApplicationBackend.dao.UserDAO;
import com.Abir.weatherApplicationBackend.dto.User;
import com.Abir.weatherApplicationBackend.model.UserModel;

/**
 * The GlobalController class is a controller advice which stores the logged in
 * user information in the httpsession object and returns old object if already
 * existing
 *
 * @author Abir Sinha
 * @version 1.0
 * @since 2019-04-16
 */
@ControllerAdvice
public class GlobalController {
	
	private static final Logger logger = LoggerFactory.getLogger(GlobalController.class);

	@Autowired
	private HttpSession session;

	@Autowired
	private UserDAO userDAO;

	private UserModel userModel = null;

	/**
	 * This method gets the logged in username details from the
	 * SecurityContextHolder and fetches all the relevant details of that user
	 * and stores the same in the httpsession object
	 * 
	 * @return UserModel This returns UserModel object consisting of logged in
	 *         User details.
	 */
	@ModelAttribute("userModel")
	public UserModel getUserModel() {
		if (session.getAttribute("userModel") == null) {
			logger.info("Inside GlobalController--> getUserModel");
			// Get the authentication details
			Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
			// fetch all the details of the logged in user
			User user = userDAO.getByEmail(authentication.getName());
			if (user != null) {
				// create a new User Model object to pass user detail
				userModel = new UserModel();
				userModel.setEmail(user.getEmail());
				userModel.setRole(user.getRole());
				session.setAttribute("userModel", userModel);
				return userModel;
			}
		}
		return (UserModel) session.getAttribute("userModel");
	}

}
