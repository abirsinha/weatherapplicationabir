package com.Abir.weatherApplication.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.Abir.weatherApplicationBackend.dao.WeatherDAO;
import com.Abir.weatherApplicationBackend.dto.WeatherHistory;
import com.Abir.weatherApplicationBackend.model.UserModel;

/**
 * The JsonDataController fetches data based on user role to be displayed to datatable for the logged in user.
 *
 * @author Abir Sinha
 * @version 1.0
 * @since 2019-04-15
 */
@Controller
@RequestMapping("/json/data")
public class JsonDataController {
	
	private static final Logger logger = LoggerFactory.getLogger(JsonDataController.class);
	
	@Autowired
	WeatherDAO weatherDAO;
	@Autowired
	private HttpSession session;
	
	/**
	 * This method fetches data 
	 * 
	 * @return List This returns a list object in json format consisting of all weather information for the logged in user.
	 */
	@RequestMapping("/all/Weather/Report")
	@ResponseBody
	public List<WeatherHistory> getAllProducts(){
		logger.info("Inside JsonDataController--> getAllProducts");
		List<WeatherHistory> weatherHistoryList= new ArrayList<>();
		UserModel userModel=(UserModel) session.getAttribute("userModel");
		//If user is admin display weather information across users else only for the logged in user
		if(userModel.getRole().equalsIgnoreCase("ADMIN")){
			weatherHistoryList=weatherDAO.getAllWeatherInformation();
		}else{
			weatherHistoryList=weatherDAO.getWeatherInformationByUserId(userModel.getEmail());
		}		
		return weatherHistoryList;
	}
	
	@RequestMapping("/weather/{id}/Report")
	@ResponseBody
	public List<WeatherHistory> getAllProducts(@PathVariable String id){
		logger.info("Inside JsonDataController--> getAllProducts having Id as pathVariable");
		return weatherDAO.getWeatherInformationByUserId(id);
	}
	
}
