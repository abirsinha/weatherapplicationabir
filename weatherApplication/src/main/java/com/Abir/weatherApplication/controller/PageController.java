package com.Abir.weatherApplication.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.logout.SecurityContextLogoutHandler;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

/**
 * The Pagecontroller class handles all the login related issue faced like
 * logging in with incorrect credentials, access denied and logout successfully.
 *
 * @author Abir Sinha
 * @version 1.0
 * @since 2019-04-16
 */

@Controller
public class PageController {

	private static final Logger logger = LoggerFactory.getLogger(PageController.class);

	/**
	 * This method maps the login url and displays error message if invalid user
	 * name or password is supplied. Also performs giving logout successful
	 * message.
	 * 
	 * @param error
	 *            This is the first parameter to capture any login related error
	 *            reported in request param
	 * @param logout
	 *            This is the second parameter to capture any logout related
	 *            error reported in request param
	 * @return ModelAndView This returns ModelAndView object.
	 */
	@RequestMapping(value = "/login")
	public ModelAndView login(@RequestParam(name = "error", required = false) String error,
			@RequestParam(name = "logout", required = false) String logout) {

		logger.info("Inside PageController--> login");

		ModelAndView mv = new ModelAndView("login");
		if (error != null) {
			mv.addObject("message", "Invalid Username or Password!!");
		}
		if (logout != null) {
			mv.addObject("logout", "User has been sucessfully logged out!!");
		}
		mv.addObject("title", "Login");
		return mv;
	}

	/**
	 * This method maps the access-denied url and displays error message if
	 * unauthorised user user tries to access the page.
	 * 
	 * @return ModelAndView This returns ModelAndView object consisting of error
	 *         details and title to be displayed.
	 */
	@RequestMapping(value = "/access-denied")
	public ModelAndView accessDenied() {
		logger.info("Inside PageController--> accessDenied");
		ModelAndView mv = new ModelAndView("error");
		mv.addObject("title", "403-Access Denied");
		mv.addObject("errorTitle", "Unauthorised Access!!!");
		mv.addObject("errorDescription", "You are not allowed to view this page");
		return mv;
	}

	/**
	 * This method maps the perform-logout url and displays error message when
	 * user successfully logs out from the page.
	 * 
	 * @return String This returns a redirect String value to login page with
	 *         logout as request param
	 */
	@RequestMapping(value = "/perform-logout")
	public String performLogout(HttpServletRequest request, HttpServletResponse response) {
		logger.info("Inside PageController--> performLogout");
		// Fetching Authentication Object
		Authentication auth = SecurityContextHolder.getContext().getAuthentication();
		if (auth != null) {
			new SecurityContextLogoutHandler().logout(request, response, auth);
		}
		return "redirect:/login?logout";
	}

}
