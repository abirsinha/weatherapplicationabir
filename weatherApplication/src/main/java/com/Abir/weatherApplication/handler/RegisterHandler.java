package com.Abir.weatherApplication.handler;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;

import com.Abir.weatherApplication.model.RegisterModel;
import com.Abir.weatherApplicationBackend.dao.UserDAO;
import com.Abir.weatherApplicationBackend.dto.User;

/**
 * The RegisterHandler class saves new user information in database with
 * encrypted password.
 *
 * @author Abir Sinha
 * @version 1.0
 * @since 2019-04-16
 */
@Component
public class RegisterHandler {

	private static final Logger logger = LoggerFactory.getLogger(RegisterHandler.class);
	
	@Autowired
	private UserDAO userDAO;
	// Used to encrypt the user entered password.
	@Autowired
	BCryptPasswordEncoder passwordEncoder;

	/**
	 * This method is called during the on start register phase and returns
	 * RegisterModel object.
	 * 
	 * @return RegisterModel This returns a new RegisterModel object.
	 * 
	 */
	public RegisterModel init() {
		logger.info("Inside RegisterHandler--> init");
		return new RegisterModel();
	}

	/**
	 * This method is called during the personal view state of the register page
	 * and sets the user input.
	 * 
	 */
	public void addUser(RegisterModel registerModel, User user) {
		logger.info("Inside RegisterHandler--> addUser");
		registerModel.setUser(user);
	}

	/**
	 * This method is called when user clicks on Confirm button on Register
	 * page, called on Submit action state.
	 * 
	 * @return String This returns a transitionValue success after saving the
	 *         user details on database.
	 * 
	 */
	public String saveUser(RegisterModel registerModel) {
		logger.info("Inside RegisterHandler--> saveUser");
		String transitionValue = "success";
		// Fetch User
		User user = registerModel.getUser();
		if (user != null) {
			// Encode the password
			user.setPassword(passwordEncoder.encode(user.getPassword()));
			// Save the user information with encrypted data in DB
			userDAO.addUser(user);
		}
		return transitionValue;
	}

}
