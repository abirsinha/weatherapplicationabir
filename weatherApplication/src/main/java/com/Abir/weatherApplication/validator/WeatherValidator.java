package com.Abir.weatherApplication.validator;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

import com.Abir.weatherApplicationBackend.dto.WeatherHistory;

/**
 * The WeatherValidator class is a validator class which checks for the valid
 * sunrise and sunset input format.
 *
 * @author Abir Sinha
 * @version 1.0
 * @since 2019-04-14
 */
public class WeatherValidator implements Validator {

	private static final Logger logger = LoggerFactory.getLogger(WeatherValidator.class);

	/**
	 * This is a overriding method where we need to give the supported class
	 * name for the validator where the validation needs to be applied to.
	 * 
	 * @param Class
	 *            This accepts the supported class name.
	 * @return Boolean returns true if the current class is the supporting class
	 *         for the validator.
	 */
	@Override
	public boolean supports(Class<?> clazz) {
		logger.info("Inside WeatherValidator--> supports");
		return WeatherHistory.class.equals(clazz);
	}

	/**
	 * This method contains the logic to validate the sunrise and sunset input
	 * format.
	 * 
	 * @param Object
	 *            This is the first parameter which capture the input paramters
	 *            by the user.
	 * @param Errors
	 *            The second parameter is used to reject the value if there is
	 *            any formatting issue with the input provided by user.
	 */
	@Override
	public void validate(Object target, Errors errors) {
		logger.info("Inside WeatherValidator--> validate");
		WeatherHistory dateWeather = (WeatherHistory) target;
		String dateToValidateSunrise = dateWeather.getSunrise();
		String dateToValidateSunset = dateWeather.getSunset();
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
		sdf.setLenient(false);

		try {
			// if not valid, it will throw ParseException
			Date dateSunrise = sdf.parse(dateToValidateSunrise);
		} catch (ParseException e) {
			// Reject the value if unsupported format
			errors.rejectValue("sunrise", null, "Please enter valid date in dd/MM/yyyy HH:mm:ss format");
			return;
		}

		try {
			// if not valid, it will throw ParseException
			Date dateSunset = sdf.parse(dateToValidateSunset);
		} catch (ParseException e) {
			// Reject the value if unsupported format
			errors.rejectValue("sunset", null, "Please enter valid date in dd/MM/yyyy HH:mm:ss format");
			return;
		}

	}

}
