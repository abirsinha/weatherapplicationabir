package com.Abir.weatherApplication.exception;

import java.io.Serializable;

public class WeatherDetailsNotFoundException extends Exception implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private String message;

	public WeatherDetailsNotFoundException() {
		this("Weather Details cannot be found or City name is incorrect!!");
	}

	public WeatherDetailsNotFoundException(String message) {
		this.message = message;
	}

	/**
	 * @return the message
	 */
	public String getMessage() {
		return message;
	}
}
