package com.Abir.weatherApplication.exception;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.NoHandlerFoundException;

/**
 * The ManagementController class handles all the functionalities a user
 * performs on its logged in screen like searching City Weather details,
 * updating , deleting or bulk deleting its information.
 *
 * @author Abir Sinha
 * @version 1.0
 * @since 2019-04-14
 */
@ControllerAdvice
public class GlobalDefaultExceptionHandler {
	private static final Logger logger = LoggerFactory.getLogger(GlobalDefaultExceptionHandler.class);
	
	/**
	 * This method maps the NoHandlerFoundException and displays appropriate message to the user.
	 * 	
	 * @return ModelAndView This returns the ModelAndView object with the error page
	 *         and appropriate message.
	 */
	@ExceptionHandler(NoHandlerFoundException.class)
	public ModelAndView handlerNoHandlerFoundException() {
		logger.info("Inside GlobalDefaultExceptionHandler--> handlerNoHandlerFoundException");
		ModelAndView mv = new ModelAndView("error");
		mv.addObject("errorTitle", "The page is not constructed!!");
		mv.addObject("errorDescription", "The page you are looking for is not available right now!");
		mv.addObject("title", "404 Error Page");
		return mv;

	}

	/**
	 * This method maps the WeatherDetailsNotFoundException and displays appropriate message to the user.
	 * 	
	 * @return ModelAndView This returns the ModelAndView object with the error page
	 *         and appropriate message.
	 */
	@ExceptionHandler(WeatherDetailsNotFoundException.class)
	public ModelAndView handlerWeatherDetailsNotFoundException() {
		logger.info("Inside GlobalDefaultExceptionHandler--> handlerWeatherDetailsNotFoundException");
		ModelAndView mv = new ModelAndView("error");
		mv.addObject("errorTitle", "Weather Details not found for the given city");
		mv.addObject("errorDescription", "Weather Details cannot be found or provided City name is incorrect!!");
		mv.addObject("title", "Weather Details unavailable");
		return mv;

	}

}
