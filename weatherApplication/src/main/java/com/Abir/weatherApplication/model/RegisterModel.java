package com.Abir.weatherApplication.model;

import java.io.Serializable;

import com.Abir.weatherApplicationBackend.dto.User;

/**
 * This is a RegisterModel class with User setter and getter methods.
 *
 * @author Abir Sinha
 * @version 1.0
 * @since 2019-04-15
 */
public class RegisterModel implements Serializable{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private User user;

	/**
	 * @return the user
	 */
	public User getUser() {
		return user;
	}

	/**
	 * @param user the user to set
	 */
	public void setUser(User user) {
		this.user = user;
	}
	
	
	
}
