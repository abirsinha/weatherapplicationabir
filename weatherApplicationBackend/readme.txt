This project contains code pertaining to back end of the application.

Technology stack of Backend
1. Java 8
2. Spring Mvc
3. Spring Web-Flow(Registration page)
4. Spring Security(Login page)
5. ORM framework--> Hibernate v5 with Spring ORM
6. Database--> H2
7. Logging framework--> slf4j
8. Validator--> Spring Validator and Hibernate Validator

Test classes:
1.Junit v4.12
2.Hamcrest v1.3