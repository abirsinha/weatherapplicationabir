CREATE TABLE User_Weather_Data (	
    weatherId identity,
	city VARCHAR(50) ,
	weather_description VARCHAR(250),
	current_temp VARCHAR(50),
	min_temp VARCHAR(50),
	max_temp VARCHAR(50),
	sunrise VARCHAR(50),
	sunset VARCHAR(50),
	userId VARCHAR(100) ,
	createdOn DATE,
	CONSTRAINT pk_weather_id PRIMARY KEY (weatherId),
	CONSTRAINT fk_Weather_user_id FOREIGN KEY (userId ) REFERENCES user_detail (email),
);


CREATE TABLE user_detail (	
	email VARCHAR(100),
	DOB Date,
	password VARCHAR(60),
	role VARCHAR(50),
	enabled BOOLEAN,
	CONSTRAINT pk_user_name_dob PRIMARY KEY(email,dob)
);




