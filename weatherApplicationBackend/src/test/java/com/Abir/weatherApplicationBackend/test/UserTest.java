package com.Abir.weatherApplicationBackend.test;

import static org.junit.Assert.assertEquals;

import java.util.Date;

import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import com.Abir.weatherApplicationBackend.dao.UserDAO;
import com.Abir.weatherApplicationBackend.dto.User;

/**
 * This is a test class of UserDAO.
 *
 * @author Abir Sinha
 * @version 1.0
 * @since 2019-04-20
 */
public class UserTest {

	private static AnnotationConfigApplicationContext context;

	private static UserDAO userDAO;

	private User user;

	/**
	 * This is a before class method which holds all the initialization required
	 * before the actual test runs.
	 */
	@BeforeClass
	public static void init() {
		context = new AnnotationConfigApplicationContext();
		context.scan("com.Abir.weatherApplicationBackend");
		context.refresh();
		userDAO = (UserDAO) context.getBean("UserDAO");
	}

	/**
	 * This is a test method for adding a new user.
	 */
	@Test
	public void testAddUser() {
		user =new User();
		user.setEmail("test@gmail.com");
		user.setDob(new Date());
		user.setPassword("abcd");
		assertEquals("Succesfully added new user in the table!", true, userDAO.addUser(user));
	}

	/**
	 * This is a test method for extracting user details by email id.
	 */
	@Test
	public void testGetByEmail() {
		user = new User();
		user =userDAO.getByEmail("test@gmail.com");
		assertEquals("Succesfully added new user in the table!", "test@gmail.com", user.getEmail());
	}

	
}
