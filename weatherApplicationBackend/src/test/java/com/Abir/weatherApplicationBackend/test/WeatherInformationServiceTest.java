package com.Abir.weatherApplicationBackend.test;

import static org.junit.Assert.assertEquals;

import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import com.Abir.weatherApplicationBackend.model.UserModel;
import com.Abir.weatherApplicationBackend.service.WeatherInformationService;

/**
 * This is a test class of WeatherInformationService.
 *
 * @author Abir Sinha
 * @version 1.0
 * @since 2019-04-20
 */
public class WeatherInformationServiceTest { 

	private static AnnotationConfigApplicationContext context;
	
	private UserModel userModel;

	private static WeatherInformationService weatherInformationService;

	/**
	 * This is a before class method which holds all the initialization required
	 * before the actual test runs.
	 */
	@BeforeClass
	public static void init() {
		context = new AnnotationConfigApplicationContext();
		context.scan("com.Abir.weatherApplicationBackend");
		context.refresh();
		weatherInformationService = (WeatherInformationService) context.getBean("weatherInformationService");
	}

	/**
	 * This is a test method for getting weather information for supplied city.
	 */
	@Test
	public void testGetWeatherInformation() {
		userModel = new UserModel();
		userModel.setEmail("Test@gmail.com");
		assertEquals("Succesfully added new user in the table!", Integer.valueOf(200),
				weatherInformationService.getWeatherInformation("Kolkata", userModel));
	}

}
