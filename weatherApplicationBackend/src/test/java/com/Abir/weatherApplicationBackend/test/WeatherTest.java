package com.Abir.weatherApplicationBackend.test;

import static org.junit.Assert.assertEquals;
import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.MatcherAssert.assertThat;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import org.hamcrest.collection.IsEmptyCollection;
import org.junit.BeforeClass;
import org.junit.Test;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import com.Abir.weatherApplicationBackend.dao.WeatherDAO;
import com.Abir.weatherApplicationBackend.dto.WeatherHistory;

/**
 * This is a test class of weatherDAO.
 *
 * @author Abir Sinha
 * @version 1.0
 * @since 2019-04-20
 */
public class WeatherTest {

	private static AnnotationConfigApplicationContext context;

	private static WeatherDAO weatherDAO;

	private WeatherHistory weatherHistory;

	/**
	 * This is a before class method which holds all the initialization required
	 * before the actual test runs.
	 */
	@BeforeClass
	public static void init() {
		context = new AnnotationConfigApplicationContext();
		context.scan("com.Abir.weatherApplicationBackend");
		context.refresh();
		weatherDAO = (WeatherDAO) context.getBean("weatherDAO"); 
	}

	/**
	 * This is a test method for Adding a new Weather details.
	 */
	@Test
	public void testAddWeather() {
		weatherHistory = new WeatherHistory();
		weatherHistory.setCity("Pune");
		weatherHistory.setCurrentTemp("32.3");
		weatherHistory.setDescription("From Test class");
		weatherHistory.setMaxTemp("34");
		weatherHistory.setMinTemp("30.2");
		weatherHistory.setSunrise("20/04/2019 17:12:21");
		weatherHistory.setSunset("21/04/2019 06:37:40");
		weatherHistory.setUserId("TestUser@gmail.com"); 
		weatherHistory.setCreatedOn(new Date());
		assertEquals("Succesfully added new weather information in the table!", true, weatherDAO.add(weatherHistory));

	}

	/**
	 * This is a test method for getting all the weather information.
	 */
	@Test
	public void testGetAllWeatherInformation() {
		List<WeatherHistory> listWeatherHistory = weatherDAO.getAllWeatherInformation();
		assertThat(listWeatherHistory, not(IsEmptyCollection.empty()));
	}

	/**
	 * This is a test method for getting all the weather information by logged
	 * in user id.
	 */
	@Test
	public void testGetWeatherInformationByUserId() {
		List<WeatherHistory> listWeatherHistory = weatherDAO.getWeatherInformationByUserId("TestUser@gmail.com");
		assertThat(listWeatherHistory, not(IsEmptyCollection.empty()));
	}

	/**
	 * This is a test method for getting the weather information by id.
	 */
	@Test
	public void testGetWeatherInformationById() {
		List<WeatherHistory> listWeatherHistory = weatherDAO.getWeatherInformationByUserId("TestUser@gmail.com");
		weatherHistory = new WeatherHistory();
		weatherHistory = listWeatherHistory.get(0);
		assertEquals("Succesfully extracted details from table for given id!", "Bhopal",
				weatherDAO.getWeatherInformationById(weatherHistory.getWeatherId()).getCity());
	}

	/**
	 * This is a test method for updating the existing weather information.
	 */
	@Test
	public void testUpdate() {
		weatherHistory = new WeatherHistory();
		List<WeatherHistory> listWeatherHistory = weatherDAO.getWeatherInformationByUserId("TestUser@gmail.com");
		weatherHistory = listWeatherHistory.get(0);
		weatherHistory.setDescription("Updated test description from TestUpdate");
		assertEquals("Succesfully updated weather information in the table!", true, weatherDAO.update(weatherHistory));
	}

	/**
	 * This is a test method for deleting the existing weather information.
	 */
	@Test
	public void testDelete() {
		int successDelete = 1;
		weatherHistory = new WeatherHistory();
		List<WeatherHistory> listWeatherHistory = weatherDAO.getWeatherInformationByUserId("TestUser@gmail.com");
		weatherHistory = listWeatherHistory.get(0);
		assertEquals("Succesfully deleted weather information in the table!", successDelete,
				weatherDAO.delete(weatherHistory.getWeatherId()));
	}

	/**
	 * This is a test method for bulk deleting the existing weather information.
	 */
	@Test
	public void testBulkDelete() {
		List<Integer> listWeatherIds = new ArrayList<Integer>();
		List<WeatherHistory> listWeatherHistory = weatherDAO.getWeatherInformationByUserId("TestUser@gmail.com");
		listWeatherHistory.forEach(weather -> listWeatherIds.add(weather.getWeatherId()));
		assertEquals("Succesfully bulk deleted weather information in the table!", listWeatherHistory.size(),
				weatherDAO.deleteBulkData(listWeatherIds));
	}

	/**
	 * This is a test method for checking if the given city is already existing
	 * in the database.
	 */
	@Test
	public void testCheckCityExistence() {
		assertEquals("Succesfully checked existing city's weather information in the table!", Integer.valueOf(1),
				weatherDAO.checkCityExistence("Pune", "TestUser@gmail.com"));
	}

}
