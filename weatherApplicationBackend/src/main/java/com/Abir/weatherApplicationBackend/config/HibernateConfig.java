package com.Abir.weatherApplicationBackend.config;

import java.util.Properties;

import javax.sql.DataSource;

import org.apache.commons.dbcp2.BasicDataSource;
import org.hibernate.SessionFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.orm.hibernate5.HibernateTransactionManager;
import org.springframework.orm.hibernate5.LocalSessionFactoryBuilder;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * The HibernateConfig class holds all the database related configurations.
 *
 * @author Abir Sinha
 * @version 1.0
 * @since 2019-04-13
 */
@Configuration
@ComponentScan(basePackages = { "com.Abir.weatherApplicationBackend.dto" })
@EnableTransactionManagement
public class HibernateConfig { 
	private static final String DATABASE_URL = "jdbc:h2:tcp://localhost/~/onlineshopping";
	private static final String DATABASE_DRIVER = "org.h2.Driver";
	private static final String DATABASE_DIALECT = "org.hibernate.dialect.H2Dialect";
	private static final String DATABASE_USERNAME = "sa";
	private static final String DATABASE_PASSWORD = "";

	/**
	 * This method returns the datasource configurations.
	 * 	
	 * @return boolean This returns the datasource configurations.
	 */
	@Bean("dataSource")
	public DataSource getDataSource() {
		BasicDataSource dataSource = new BasicDataSource();
		dataSource.setUrl(DATABASE_URL);
		dataSource.setDriverClassName(DATABASE_DRIVER);
		dataSource.setUsername(DATABASE_USERNAME);
		dataSource.setPassword(DATABASE_PASSWORD);
		return dataSource;
	}

	/**
	 * This method returns the Session Factory given the datasource information.
	 * 
	 * @param DataSource
	 *            This is the parameter which holds all datasource configuration details.
	 * @return boolean This returns true if the data is persisted successfully
	 *         in the table else returns false.
	 */
	@Bean
	public SessionFactory getSessionFactory(DataSource dataSource) {
		LocalSessionFactoryBuilder builder = new LocalSessionFactoryBuilder(dataSource);
		builder.addProperties(getHibernateProperties());
		builder.scanPackages("com.Abir.weatherApplicationBackend.dto");
		return builder.buildSessionFactory();
	}

	/**
	 * This is a private method to get the hibernate properties.
	 * 	
	 * @return Properties This returns the hibernate properties.
	 */
	private Properties getHibernateProperties() {
		Properties properties = new Properties();
		properties.put("hibernate.dialect", DATABASE_DIALECT);
		properties.put("hibernate.show_sql", "true");
		properties.put("hibernate.format_sql", "true");
		return properties;
	}

	/**
	 * This method returns the HibernateTransactionManager provided the SessionFactory.
	 * 
	 * @param SessionFactory
	 *            This is the parameter which holds all datasource configuration details.
	 * @return HibernateTransactionManager This returns HibernateTransactionManager.
	 */
	@Bean
	public HibernateTransactionManager getTransactionManager(SessionFactory sessionFactory) {
		HibernateTransactionManager transactionManager = new HibernateTransactionManager(sessionFactory);
		return transactionManager;
	}

}
