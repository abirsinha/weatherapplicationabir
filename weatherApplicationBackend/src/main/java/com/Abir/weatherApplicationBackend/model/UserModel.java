package com.Abir.weatherApplicationBackend.model;

import java.io.Serializable;

/**
 * The UserModel class holds the email and role data for the user.
 *
 * @author Abir Sinha
 * @version 1.0
 * @since 2019-04-14
 */
public class UserModel implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String email;
	private String role;
	/**
	 * @return the email
	 */
	public String getEmail() {
		return email;
	}
	/**
	 * @param email the email to set
	 */
	public void setEmail(String email) {
		this.email = email;
	}
	/**
	 * @return the role
	 */
	public String getRole() {
		return role;
	}
	/**
	 * @param role the role to set
	 */
	public void setRole(String role) {
		this.role = role;
	}
	
	@Override
	public String toString() {
		return "UserModel [email=" + email + ", role=" + role + "]";
	}

}
