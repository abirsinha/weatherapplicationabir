package com.Abir.weatherApplicationBackend.dao;

import com.Abir.weatherApplicationBackend.dto.User;

/**
 * The UserDAOImpl class handles all the database related operations
 * pertaining to User table.
 *
 * @author Abir Sinha
 * @version 1.0
 * @since 2019-04-15
 */
public interface UserDAO {	

	/**
	 * This method adds the new user in the table.
	 * 
	 * @param User
	 *            This is the parameter which holds all the user details to
	 *            be persisted in User table.
	 * @return boolean This returns true if the data is persisted successfully
	 *         in the table else returns false.
	 */
	boolean addUser(User user);

	/**
	 * This method gets the user details from the table given the email Id.
	 * 
	 * @param emailId
	 *            This is the parameter which the user Email id.
	 * @return User This returns the User object for the given email Id.
	 */
	User getByEmail(String email);
}
