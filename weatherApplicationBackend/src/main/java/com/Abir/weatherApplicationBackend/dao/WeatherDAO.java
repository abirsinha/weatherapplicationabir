package com.Abir.weatherApplicationBackend.dao;

import java.util.List;

import com.Abir.weatherApplicationBackend.dto.WeatherHistory;

/**
 * The WeatherDAO interface handles all the database related operations
 * pertaining to Weather History table.
 *
 * @author Abir Sinha
 * @version 1.0
 * @since 2019-04-14
 */
public interface WeatherDAO {
	/**
	 * This method adds the new weather history data in the table.
	 * 
	 * @param WeatherHistory
	 *            This is the parameter which holds all the weather details to
	 *            be persisted in weather History table.
	 * @return boolean This returns true if the data is persisted successfully
	 *         in the table else returns false.
	 */
	boolean add(WeatherHistory weather);

	/**
	 * This method is used to extract all the records from the WeatherHistory
	 * table.
	 * 
	 * @return List<WeatherHistory> This returns the list of all the
	 *         WeatherHistory in the tables.
	 */
	List<WeatherHistory> getAllWeatherInformation();

	/**
	 * This method returns the list of all the WeatherHistory in the tables for
	 * the entered userId.
	 * 
	 * @param userId
	 *            This is the parameter which holds the logged in username.
	 * @return List<WeatherHistory> This returns the list of all the
	 *         WeatherHistory in the tables for the entered username.
	 */
	List<WeatherHistory> getWeatherInformationByUserId(String id);

	/**
	 * This method returns the WeatherHistory object given the weatherId.
	 * 
	 * @param id
	 *            This is the parameter which holds the weather Id.
	 * @return WeatherHistory This returns the WeatherHistory object.
	 */
	WeatherHistory getWeatherInformationById(int id);
	
	/**
	 * This method updates the weather history object.
	 * 
	 * @param WeatherHistory
	 *            This is the parameter which holds the updated weatherHistory
	 *            object.
	 * @return boolean This returns true if updated succesfully else returns
	 *         false.
	 */
	boolean update(WeatherHistory weather);

	/**
	 * This method deletes the weather history object given the id.
	 * 
	 * @param id
	 *            This is the parameter which holds the weather Id to be
	 *            deleted.
	 * @return int returns positive value if deleted successfully else returns
	 *         0.
	 */
	int delete(int id);

	/**
	 * This method bulk deletes the given weather ids.
	 * 
	 * @param id
	 *            This is the parameter which holds the weather Id to be
	 *            deleted.
	 * @return int returns positive value if deleted successfully else returns
	 *         0.
	 */
	int deleteBulkData(List<Integer> weatherIds);

	/**
	 * This method checks if the searched city has already been searched by user
	 * earlier.
	 * 
	 * @param city
	 *            This is the first parameter which holds the searched city.
	 * @param userId
	 *            This is the second parameter which holds the logged in user
	 *            Id.
	 * @return Integer returns positive value(1), if searched city is already
	 *         existing in table.
	 */
	Integer checkCityExistence(String city, String userId);
}
