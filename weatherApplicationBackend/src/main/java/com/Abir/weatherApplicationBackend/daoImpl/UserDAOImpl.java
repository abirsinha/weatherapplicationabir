package com.Abir.weatherApplicationBackend.daoImpl;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.Abir.weatherApplicationBackend.dao.UserDAO;
import com.Abir.weatherApplicationBackend.dto.User;

/**
 * The UserDAOImpl class handles all the database related operations
 * pertaining to User table.
 *
 * @author Abir Sinha
 * @version 1.0
 * @since 2019-04-15
 */
@Repository("UserDAO")
@Transactional
public class UserDAOImpl implements UserDAO {  

	@Autowired
	private SessionFactory sessionFactory;
	
	/**
	 * This method adds the new user in the table.
	 * 
	 * @param User
	 *            This is the parameter which holds all the user details to
	 *            be persisted in User table.
	 * @return boolean This returns true if the data is persisted successfully
	 *         in the table else returns false.
	 */
	@Override
	public boolean addUser(User user) {
		try {
			sessionFactory.getCurrentSession().persist(user);
			return true;
		} catch (Exception ex) {
			return false;
		}
	}

	/**
	 * This method gets the user details from the table given the email Id.
	 * 
	 * @param emailId
	 *            This is the parameter which the user Email id.
	 * @return User This returns the User object for the given email Id.
	 */
	@Override
	public User getByEmail(String email) {
		String selectQuery = "FROM User WHERE email = :email";
		try {
			return sessionFactory.getCurrentSession().createQuery(selectQuery, User.class).setParameter("email", email)
					.getSingleResult();
		} catch (Exception ex) {
			return null;
		}

	}	

}
