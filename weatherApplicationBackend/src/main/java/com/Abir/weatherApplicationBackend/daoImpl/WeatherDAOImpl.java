package com.Abir.weatherApplicationBackend.daoImpl;

import java.util.List;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import com.Abir.weatherApplicationBackend.dao.WeatherDAO;
import com.Abir.weatherApplicationBackend.dto.WeatherHistory;

/**
 * The WeatherDAOImpl class handles all the database related operations
 * pertaining to Weather History table.
 *
 * @author Abir Sinha
 * @version 1.0
 * @since 2019-04-14
 */
@Repository("weatherDAO")
@Transactional
public class WeatherDAOImpl implements WeatherDAO {

	@Autowired
	private SessionFactory sessionFactory; 

	/**
	 * This method adds the new weather history data in the table.
	 * 
	 * @param WeatherHistory
	 *            This is the parameter which holds all the weather details to
	 *            be persisted in weather History table.
	 * @return boolean This returns true if the data is persisted successfully
	 *         in the table else returns false.
	 */
	@Override
	public boolean add(WeatherHistory weather) {
		try {
			sessionFactory.getCurrentSession().persist(weather);
			return true;
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return false;
	}

	/**
	 * This method is used to extract all the records from the WeatherHistory
	 * table.
	 * 
	 * @return List<WeatherHistory> This returns the list of all the
	 *         WeatherHistory in the tables.
	 */
	@Override
	public List<WeatherHistory> getAllWeatherInformation() {
		return sessionFactory.getCurrentSession().createQuery("FROM WeatherHistory", WeatherHistory.class)
				.getResultList();
	}

	/**
	 * This method returns the list of all the WeatherHistory in the tables for
	 * the entered userId.
	 * 
	 * @param userId
	 *            This is the parameter which holds the logged in username.
	 * @return List<WeatherHistory> This returns the list of all the
	 *         WeatherHistory in the tables for the entered username.
	 */
	@Override
	public List<WeatherHistory> getWeatherInformationByUserId(String userId) {
		String weatherInformationByUserId = "FROM WeatherHistory WHERE userId = :userId";
		return sessionFactory.getCurrentSession().createQuery(weatherInformationByUserId, WeatherHistory.class)
				.setParameter("userId", userId).getResultList();
	}

	/**
	 * This method returns the WeatherHistory object given the weatherId.
	 * 
	 * @param id
	 *            This is the parameter which holds the weather Id.
	 * @return WeatherHistory This returns the WeatherHistory object.
	 */
	@Override
	public WeatherHistory getWeatherInformationById(int id) {
		return sessionFactory.getCurrentSession().get(WeatherHistory.class, Integer.valueOf(id));
	}

	/**
	 * This method updates the weather history object.
	 * 
	 * @param WeatherHistory
	 *            This is the parameter which holds the updated weatherHistory
	 *            object.
	 * @return boolean This returns true if updated succesfully else returns
	 *         false.
	 */
	@Override
	public boolean update(WeatherHistory weather) {
		try {
			sessionFactory.getCurrentSession().update(weather);
			return true;
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return false;
	}

	/**
	 * This method deletes the weather history object given the id.
	 * 
	 * @param id
	 *            This is the parameter which holds the weather Id to be
	 *            deleted.
	 * @return int returns positive value if deleted successfully else returns
	 *         0.
	 */
	@Override
	public int delete(int id) {
		String weatherInformationByUserId = "Delete FROM WeatherHistory WHERE weatherId = :weatherId";
		return sessionFactory.getCurrentSession().createQuery(weatherInformationByUserId).setParameter("weatherId", id)
				.executeUpdate();
	}

	/**
	 * This method bulk deletes the given weather ids.
	 * 
	 * @param id
	 *            This is the parameter which holds the weather Id to be
	 *            deleted.
	 * @return int returns positive value if deleted successfully else returns
	 *         0.
	 */
	@Override
	public int deleteBulkData(List<Integer> weatherIds) {
		String weatherInformationByUserId = "Delete FROM WeatherHistory WHERE weatherId in(:weatherIds) ";
		return sessionFactory.getCurrentSession().createQuery(weatherInformationByUserId)
				.setParameter("weatherIds", weatherIds).executeUpdate();
	}

	/**
	 * This method checks if the searched city has already been searched by user
	 * earlier.
	 * 
	 * @param city
	 *            This is the first parameter which holds the searched city.
	 * @param userId
	 *            This is the second parameter which holds the logged in user
	 *            Id.
	 * @return Integer returns positive value(1), if searched city is already
	 *         existing in table.
	 */
	@Override
	public Integer checkCityExistence(String city, String userId) {
		Integer cityCount = 0;
		String selectQuery = "select count(*) FROM WeatherHistory WHERE userId = :userId and city=:city";
		try {
			cityCount = (Integer) sessionFactory.getCurrentSession().createQuery(selectQuery, Long.class)
					.setParameter("userId", userId).setParameter("city", city).getSingleResult().intValue();

		} catch (Exception ex) {

		}
		return cityCount;
	}

}
