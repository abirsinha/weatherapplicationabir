package com.Abir.weatherApplicationBackend.service;

import com.Abir.weatherApplicationBackend.model.UserModel;

/**
 * The WeatherInformationService interface fetches the real time weather information given the city.
 *
 * @author Abir Sinha
 * @version 1.0
 * @since 2019-04-15
 */
public interface WeatherInformationService {
	
	/**
	 * This method fetches the real time weather details for the given city.
	 * This makes the actual Rest APi call to fetch the data.
	 * 
	 * @param city
	 *            This is the first parameter which holds the city name supplied
	 *            by logged in user.
	 * @param userModel
	 *            This is the second parameter which holds the logged in user
	 *            details.
	 * 
	 * @return Integer This returns response code.Response code = 200 signifies
	 *         successful fetch.
	 */
	public Integer getWeatherInformation(String city, UserModel userModel);
}