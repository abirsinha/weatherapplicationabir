package com.Abir.weatherApplicationBackend.serviceImpl;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.stream.Collectors;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.Abir.weatherApplicationBackend.dao.WeatherDAO;
import com.Abir.weatherApplicationBackend.dto.WeatherHistory;
import com.Abir.weatherApplicationBackend.dto.WeatherInformation;
import com.Abir.weatherApplicationBackend.model.UserModel;
import com.Abir.weatherApplicationBackend.service.WeatherInformationService;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * The WeatherInformationServiceImpl class fetches the real time weather
 * information given the city.
 *
 * @author Abir Sinha
 * @version 1.0
 * @since 2019-04-15
 */
@Service("weatherInformationService")
public class WeatherInformationServiceImpl implements WeatherInformationService {

	@Autowired
	private WeatherDAO weatherDAO;

	/**
	 * This method fetches the real time weather details for the given city. 
	 * This makes the actual Rest APi call to fetch the data.
	 * 
	 * @param city
	 *            This is the first parameter which holds the city name supplied
	 *            by logged in user.
	 * @param userModel
	 *            This is the second parameter which holds the logged in user
	 *            details.
	 * 
	 * @return Integer This returns response code.Response code = 200 signifies
	 *         successful fetch.
	 */
	@Override
	public Integer getWeatherInformation(String city, UserModel userModel) {

		DefaultHttpClient httpClient = new DefaultHttpClient();
		HttpGet getRequest = new HttpGet("https://api.openweathermap.org/data/2.5/weather?q=" + city
				+ "&units=metric&appid=4f72e53f67af8d1317adf9681c2ec9c0");
		getRequest.addHeader("accept", "application/json"); 
		Integer responseCode = 0;
		try {
			HttpResponse response = httpClient.execute(getRequest);
			responseCode = response.getStatusLine().getStatusCode();
			if (response.getStatusLine().getStatusCode() != 200) {
				throw new RuntimeException("Failed : HTTP error code : " + response.getStatusLine().getStatusCode());
			}
			BufferedReader br = new BufferedReader(new InputStreamReader((response.getEntity().getContent())));
			System.out.println("Output from Server .... \n");
			String jsonData = br.lines().collect(Collectors.joining());
			System.out.println(jsonData);

			if (null != jsonData) {
				convertToJavaObject(jsonData, city, userModel);
			}

			httpClient.getConnectionManager().shutdown();

		} catch (ClientProtocolException e) {

			e.printStackTrace();
		} catch (Exception e) {

			e.printStackTrace();
		}

		return responseCode;

	}

	/**
	 * This private method converts the fetched json String to java object which
	 * in turn needs to be persisted in database.
	 * 
	 * @param jsonData
	 *            This is the first parameter which holds the json String
	 *            returned from the Rest Api call.
	 * @param city
	 *            This is the second parameter which holds the city name
	 *            supplied by logged in user.
	 * @param userModel
	 *            This is the third parameter which holds the logged in user
	 *            details.
	 */
	private void convertToJavaObject(String jsonData, String city, UserModel userModel) {

		ObjectMapper mapper = new ObjectMapper();
		mapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);
		try {
			WeatherInformation weatherDetails = mapper.readValue(jsonData, WeatherInformation.class);

			if (null != weatherDetails) {
				populateWeatherInformation(weatherDetails, city, userModel);

			}

		} catch (JsonMappingException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	/**
	 * This private method persists the data in the database after formatting
	 * the same.
	 * 
	 * @param jsonData
	 *            This is the first parameter which holds the json String
	 *            returned from the Rest Api call.
	 * @param city
	 *            This is the second parameter which holds the city name
	 *            supplied by logged in user.
	 * @param userModel
	 *            This is the third parameter which holds the logged in user
	 *            details.
	 */
	private void populateWeatherInformation(WeatherInformation weatherDetails, String city, UserModel userModel) {

		WeatherHistory weatherHistory = new WeatherHistory();

		weatherHistory.setCity(city);
		weatherHistory.setCurrentTemp(weatherDetails.getMain().getTemp());
		weatherHistory.setMaxTemp(weatherDetails.getMain().getTemp_max());
		weatherHistory.setMinTemp(weatherDetails.getMain().getTemp_min());
		weatherHistory.setDescription(weatherDetails.getWeather().get(0).getDescription());
		weatherHistory.setUserId(userModel.getEmail());
		String sunrise = weatherDetails.getSys().getSunrise();
		// Convert the sunrise in dd/MM/yyyy HH:mm:ss format
		if (null != sunrise) {
			String dateString = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss")
					.format(new Date(Long.parseLong(sunrise) * 1000));
			weatherHistory.setSunrise(dateString);

		}
		String sunset = weatherDetails.getSys().getSunset();
		// Convert the sunset in dd/MM/yyyy HH:mm:ss format
		if (null != sunset) {
			String dateString = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss")
					.format(new Date(Long.parseLong(sunset) * 1000));
			weatherHistory.setSunset(dateString);
		}

		weatherHistory.setCreatedOn(new Date());
		// Persist the data in database
		weatherDAO.add(weatherHistory);

	}

}
