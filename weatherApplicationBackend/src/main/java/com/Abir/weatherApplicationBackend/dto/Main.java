package com.Abir.weatherApplicationBackend.dto;

/**
 * The Main class is use to map the REST API Json data for fields current
 * ,minimum and maximum temperatures.
 *
 * @author Abir Sinha
 * @version 1.0
 * @since 2019-04-14
 */
public class Main {

	private String temp;
	private String pressure;
	private String humidity;
	private String temp_min;
	private String temp_max;

	// Getter Methods

	public String getTemp() {
		return temp;
	}

	public String getPressure() {
		return pressure;
	}

	public String getHumidity() {
		return humidity;
	}

	public String getTemp_min() {
		return temp_min;
	}

	public String getTemp_max() {
		return temp_max;
	}

	// Setter Methods

	public void setTemp(String temp) {
		this.temp = temp;
	}

	public void setPressure(String pressure) {
		this.pressure = pressure;
	}

	public void setHumidity(String humidity) {
		this.humidity = humidity;
	}

	public void setTemp_min(String temp_min) {
		this.temp_min = temp_min;
	}

	public void setTemp_max(String temp_max) {
		this.temp_max = temp_max;
	}
}
