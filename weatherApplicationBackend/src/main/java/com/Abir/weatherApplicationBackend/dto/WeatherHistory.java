package com.Abir.weatherApplicationBackend.dto;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Pattern;

import org.hibernate.validator.constraints.NotBlank;

/**
 * This is the WeatherHistory entity class.
 *
 * @author Abir Sinha
 * @version 1.0
 * @since 2019-04-14
 */
@Entity
@Table(name = "User_Weather_Data")
public class WeatherHistory { 

	@Id	
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer weatherId;
	
	@NotBlank(message="Please enter City Name")	
	@Pattern(regexp = "^[A-Za-z]+$", message="Please enter only String Value")
	private String city;	
	@NotBlank(message="Please enter Description")
	@Column(name="weather_description")
	private String description;
	@NotBlank(message="Please enter Current Temperature")
	@Pattern(regexp = "[+-]?([0-9]*[.])?[0-9]+", message="Please enter valid current temperature value")
	@Column(name="current_temp")
	private String currentTemp;
	@NotBlank(message="Please enter Minimum Temperature")
	@Pattern(regexp = "[+-]?([0-9]*[.])?[0-9]+", message="Please enter valid minimum temperature value")
	@Column(name="min_temp")
	private String minTemp;
	@NotBlank(message="Please enter Maximum Temperature")
	@Pattern(regexp = "[+-]?([0-9]*[.])?[0-9]+", message="Please enter valid maximum temperature value")
	@Column(name="max_temp")
	private String maxTemp;	
	@NotBlank(message="Please enter Sunrise value")
	private String sunrise;
	@NotBlank(message="Please enter sunset value")
	private String sunset;
	
	private String userId;
	
	private Date createdOn;
	
	
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getCurrentTemp() {
		return currentTemp;
	}
	public void setCurrentTemp(String currentTemp) {
		this.currentTemp = currentTemp;
	}
	public String getMinTemp() {
		return minTemp;
	}
	public void setMinTemp(String minTemp) {
		this.minTemp = minTemp;
	}
	public String getMaxTemp() {
		return maxTemp;
	}
	public void setMaxTemp(String maxTemp) {
		this.maxTemp = maxTemp;
	}
	public String getSunrise() {
		return sunrise;
	}
	public void setSunrise(String sunrise) {
		this.sunrise = sunrise;
	}
	public String getSunset() {
		return sunset;
	}
	public void setSunset(String sunset) {
		this.sunset = sunset;
	}
	public String getUserId() {
		return userId;
	}
	public void setUserId(String userId) {
		this.userId = userId;
	}
	public Date getCreatedOn() {
		return createdOn;
	}
	public void setCreatedOn(Date createdOn) {
		this.createdOn = createdOn;
	}
	public Integer getWeatherId() {
		return weatherId;
	}
	public void setWeatherId(Integer weatherId) {
		this.weatherId = weatherId;
	}
	@Override
	public String toString() {
		return "WeatherHistory [city=" + city + ", description=" + description + ", currentTemp=" + currentTemp
				+ ", minTemp=" + minTemp + ", maxTemp=" + maxTemp + ", sunrise=" + sunrise + ", sunset=" + sunset
				+ ", userId=" + userId + ", createdOn=" + createdOn + "]";
	}
	
	

}
