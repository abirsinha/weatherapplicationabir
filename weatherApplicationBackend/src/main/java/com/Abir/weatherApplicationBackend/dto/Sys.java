package com.Abir.weatherApplicationBackend.dto;

/**
 * The Sys class is use to map the REST API Json data for fields sunrise and
 * sunset.
 *
 * @author Abir Sinha
 * @version 1.0
 * @since 2019-04-14
 */
public class Sys {

	private String type;
	private String id;
	private String message;
	private String country;
	private String sunrise;
	private String sunset;

	// Getter Methods

	public String getType() {
		return type;
	}

	public String getId() {
		return id;
	}

	public String getMessage() {
		return message;
	}

	public String getCountry() {
		return country;
	}

	public String getSunrise() {
		return sunrise;
	}

	public String getSunset() {
		return sunset;
	}

	// Setter Methods

	public void setType(String type) {
		this.type = type;
	}

	public void setId(String id) {
		this.id = id;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public void setSunrise(String sunrise) {
		this.sunrise = sunrise;
	}

	public void setSunset(String sunset) {
		this.sunset = sunset;
	}
}
