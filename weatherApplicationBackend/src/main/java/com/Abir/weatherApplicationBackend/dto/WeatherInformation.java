package com.Abir.weatherApplicationBackend.dto;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * The WeatherInformation class is the master class which is used to map the REST API Json data.
 *
 * @author Abir Sinha
 * @version 1.0
 * @since 2019-04-14
 */

public class WeatherInformation {

	private Coord coord;
	private List<Weather> weather;
	private String base;
	private Main main;
	private String visibility;
	private Wind wind;
	private Clouds clouds;
	private String dt;
	private Sys sys;
	private String id;
	private String name;
	private String cod;
	
	public Coord getCoord() {
		return coord;
	}
	public void setCoord(Coord coord) {
		this.coord = coord;
	}
	public List<Weather> getWeather() {
		return weather;
	}
	public void setWeather(List<Weather> weather) {
		this.weather = weather;
	}
	public String getBase() {
		return base;
	}
	public void setBase(String base) {
		this.base = base;
	}
	public Main getMain() {
		return main;
	}
	public void setMain(Main main) {
		this.main = main;
	}
	public String getVisibility() {
		return visibility;
	}
	public void setVisibility(String visibility) {
		this.visibility = visibility;
	}
	public Wind getWind() {
		return wind;
	}
	public void setWind(Wind wind) {
		this.wind = wind;
	}
	public Clouds getClouds() {
		return clouds;
	}
	public void setClouds(Clouds clouds) {
		this.clouds = clouds;
	}
	public String getDt() {
		return dt;
	}
	public void setDt(String dt) {
		this.dt = dt;
	}
	public Sys getSys() {
		return sys;
	}
	public void setSys(Sys sys) {
		this.sys = sys;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getCod() {
		return cod;
	}
	public void setCod(String cod) {
		this.cod = cod;
	}
	
	
	
}



class Clouds {
	private String all;

	// Getter Methods

	public String getAll() {
		return all;
	}

	// Setter Methods

	public void setAll(String all) {
		this.all = all;
	}
}

class Wind {
	private String speed;
	private String deg;

	// Getter Methods

	public String getSpeed() {
		return speed;
	}

	public String getDeg() {
		return deg;
	}

	// Setter Methods

	public void setSpeed(String speed) {
		this.speed = speed;
	}

	public void setDeg(String deg) {
		this.deg = deg;
	}
}

 

class Coord {
	private String lon;
	private String lat;

	// Getter Methods

	public String getLon() {
		return lon;
	}

	public String getLat() {
		return lat;
	}

	// Setter Methods

	public void setLon(String lon) {
		this.lon = lon;
	}

	public void setLat(String lat) {
		this.lat = lat;
	}
}



